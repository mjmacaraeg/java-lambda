package com.jilbrisbane.controller;

import com.jilbrisbane.entity.Schedule;
import com.jilbrisbane.repository.ScheduleRepository;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;

@RestController
public class ScheduleController {
    private final ScheduleRepository scheduleRepository;

    public ScheduleController(ScheduleRepository scheduleRepository) {
        this.scheduleRepository = scheduleRepository;
    }

    @GetMapping(value = "/schedule", produces = MediaType.APPLICATION_JSON_VALUE)
    public Iterable<Schedule> getSchedules() {
        return scheduleRepository.findAll();
    }

    @GetMapping(value = "/schedule/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Schedule getRide(@PathVariable long id) {
        return scheduleRepository.findById(id)
                .orElseThrow(
                        () -> new ResponseStatusException(HttpStatus.NOT_FOUND, String.format("Invalid Schedule id %s", id)));
    }

    @PostMapping(value = "/schedule", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
    public Schedule postSchedule(@Valid @RequestBody Schedule schedule) {
        return scheduleRepository.save(schedule);
    }
}
