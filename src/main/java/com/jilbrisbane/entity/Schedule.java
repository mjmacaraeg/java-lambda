package com.jilbrisbane.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.NotEmpty;
import java.util.Date;


@Entity
@Getter
@ToString
@NoArgsConstructor
public class Schedule {

    @Id
    @GeneratedValue(strategy=GenerationType.AUTO)
    private Long id;
    @NotEmpty
    private String name;
    @NotEmpty
    private String description;
    private Date schedule_date;

    public Schedule(String name, String description, Date schedule_date) {
        this.name = name;
        this.description = description;
        this.schedule_date = schedule_date;
    }
}
