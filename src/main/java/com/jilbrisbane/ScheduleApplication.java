package com.jilbrisbane;

import com.jilbrisbane.entity.Schedule;
import com.jilbrisbane.repository.ScheduleRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.Date;

@SpringBootApplication
public class ScheduleApplication {

    public static void main(String[] args) {
        SpringApplication.run(ScheduleApplication.class);
    }


    @Bean
    public CommandLineRunner sampleData(ScheduleRepository repository) {
        return (args) -> {
            repository.save(new Schedule("Schedule 1", "Schedule 1.", new Date()));
            repository.save(new Schedule("Schedule 2", "Schedule 2.", new Date()));
            repository.save(new Schedule("Schedule 3", "Schedule 3.", new Date()));
            repository.save(new Schedule("Schedule 4", "Schedule 4.", new Date()));
            repository.save(new Schedule("Schedule 5", "Schedule 5.", new Date()));
            repository.save(new Schedule("Schedule 6", "Schedule 6.", new Date()));
        };
    }

}
